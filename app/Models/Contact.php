<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $name
 * @property mixed $email
 * @property mixed $message
 */
class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'message'
    ];
}
